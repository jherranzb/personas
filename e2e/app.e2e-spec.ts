import { PersonsPage } from './app.po';

describe('persons App', function() {
  let page: PersonsPage;

  beforeEach(() => {
    page = new PersonsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
