import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Person } from '../person/person';
import { ActivatedRoute, Router } from '@angular/router';
import { PeopleService } from '../people-list/people.service';
import { Response } from '@angular/http';

@Component({
  selector: 'person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})

export class PersonDetailsComponent implements OnInit, OnDestroy {

  person : Person;
  sub: any;
  professions: string[] = ['jedi', 'bounty hunter', 'princess', 'sith lord'];


    constructor(private peopleService: PeopleService,
              private route: ActivatedRoute,
              private router: Router) { }

    ngOnInit(){
        this.sub = this.route.params.subscribe(params => {
            let id = Number.parseInt(params['id']);
            console.log('getting person with id: ', id);
            this.peopleService
                .get(id)
                .subscribe(p => this.person = p);
        });
    }

      ngOnDestroy(){
        this.sub.unsubscribe();
      }

      gotoPeoplesList(){
          window.history.back();
      }

    /*savePersonDetails(){
        alert(`saved!!! ${JSON.stringify(this.person)}`);
        this.peopleService.save(this.person);
    }*/

    savePersonDetails(){
        this.peopleService
            .save(this.person)
            .subscribe(
                (r: Response) => {console.log('success');}
            );
    }

}
