import { Component, OnInit, OnDestroy } from '@angular/core';
import { Person } from '../person/person';
import { ActivatedRoute, Router } from '@angular/router';
import { PeopleService } from '../people-list/people.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-person-creation',
  templateUrl: './person-creation.component.html',
  styleUrls: ['./person-creation.component.css']
})
export class PersonCreationComponent implements OnInit, OnDestroy {

  person : Person = {id: null, name: '', weight: null, height: null, profession: ''};
  sub: any;
  professions: string[] = ['jedi', 'bounty hunter', 'princess', 'sith lord'];


  constructor(private peopleService: PeopleService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(){
    this.sub = this.route.params.subscribe();
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  gotoPeoplesList(){
    window.history.back();
  }

  /*savePersonDetails(){
   alert(`saved!!! ${JSON.stringify(this.person)}`);
   this.peopleService.save(this.person);
   }*/

  createPerson(){
    console.log(this.person);
    this.peopleService
        .new(this.person)
        .subscribe(
            (r: Response) => {console.log('success');}
        );
    let link = ['/persons'];
    this.router.navigate(link);
  }

}
