import { Routes, RouterModule } from '@angular/router';
import { PeopleListComponent } from './people-list/people-list.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { PersonCreationComponent } from './person-creation/person-creation.component';

// Route config let's you map routes to components
const routes: Routes = [
    // map '/persons' to the people list component
    {
        path: 'persons',
        component: PeopleListComponent,
    },
    // map '/' to '/persons' as our default route
    {
        path: 'persons/show/:id',
        component: PersonDetailsComponent
    },
    {
        path: 'persons/create',
        component: PersonCreationComponent
    },
    {
        path: '',
        redirectTo: '/persons',
        pathMatch: 'full'
    },
];

export const routing = RouterModule.forRoot(routes);