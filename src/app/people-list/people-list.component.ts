import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PlatformLocation } from '@angular/common'
import { Person } from '../person/person';
import { PeopleService } from './people.service';

@Component({
  selector: 'people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.css']
})
export class PeopleListComponent implements OnInit {
    people: Person[] = [];
    selectedPerson: Person;
    page: number;

    constructor(private peopleService: PeopleService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                location: PlatformLocation){
        location.onPopState(() => {
            console.log('pressed back!');
            console.log(this.activatedRoute.queryParams['page']);
        });
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            let page = params['page'];
            this.getPage(page);
        });

    }

    ngOnInit(){
    }

    getPage(page) {
        if(page == 'undefined')
            page = 1;
        if(page > 1 ){
            window.history.pushState(null, "pagetitle", "persons?page=" + page);
        } else {
            window.history.pushState(null, "pagetitle", "persons");
        }
        this.peopleService
            .getAll(page)
            .subscribe(p => this.people = p);
        this.page = page;
    }

    selectPerson(person){
        this.selectedPerson = person;
    }

    gotoCreatePerson(){
        let link = ['/persons/create'];
        this.router.navigate(link);
    }

}

