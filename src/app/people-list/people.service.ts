import { Injectable } from '@angular/core';
import { Person } from '../person/person';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

const PEOPLE : Person[] = [
    {id: 1, name: 'Luke Skywalker', height: 177, weight: 70},
    {id: 2, name: 'Darth Vader', height: 200, weight: 100},
    {id: 3, name: 'Han Solo', height: 185, weight: 85},
];

@Injectable()
export class PeopleService{
    private baseUrl: string = 'http://127.0.0.1:8000/api';
    constructor(private http : Http){
    }

    getAll(p: number): Observable<Person[]>{
        if(p == 1){
            var offset = 0;
        } else {
            var offset = (p - 1) * 5;
        }
        let people$ = this.http
            .get(`${this.baseUrl}/people.json?offset=` + offset + `&limit=5`, {headers: this.getHeaders()})
            .map(mapPersons);
        return people$;
    }

    get(id: number): Observable<Person> {
        let person$ = this.http
            .get(`${this.baseUrl}/people/${id}.json`, {headers: this.getHeaders()})
            .map(mapPerson);
        return person$;
    }

    save(person: Person) : Observable<Response>{
        // this won't actually work because the StarWars API doesn't
        // is read-only. But it would look like this:
        return this
            .http
            .put(`${this.baseUrl}/people/${person.id}.json`, JSON.stringify(person), {headers: this.getHeaders()});
    }

    new(person: Person) : Observable<Response>{
        // this won't actually work because the StarWars API doesn't
        // is read-only. But it would look like this:
        return this
            .http
            .post(`${this.baseUrl}/people.json`, JSON.stringify(person), {headers: this.getHeaders()});
    }

    private getHeaders(){
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('content-type', 'application/json');
        return headers;
    }

    /*getAll() : Person[] {
        //return PEOPLE;
        return PEOPLE.map(p => this.clone(p));
    }*/

    /*get(id: number) : Person {
        return this.clone(PEOPLE.find(p => p.id === id))
    }*/

    /*save(person: Person){
        let originalPerson = PEOPLE.find(p => p.id === person.id);
        if (originalPerson) Object.assign(originalPerson, person);
        // saved muahahaha
    }*/

    private clone(object: any){
        // hack
        return JSON.parse(JSON.stringify(object));
    }
}

function mapPersons(response:Response): Person[]{
    // The response of the API has a results
    // property with the actual results
    console.log(response.json().data.map(toPerson));
    console.log(response.json());
    return response.json().data.map(toPerson)
}

function mapPerson(response:Response): Person{
    // toPerson looks just like in the previous example
    return toPerson(response.json().data);
}

function toPerson(r:any): Person{
    let person = <Person>({
        id: r.id,
        name: r.name,
        weight: r.weight,
        height: r.height,
    });
    return person;
}

// to avoid breaking the rest of our app
// I extract the id from the person url
function extractId(personData:any){
    let extractedId = personData.url.replace('http://swapi.co/api/people/','').replace('/','');
    return parseInt(extractedId);
}